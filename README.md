# Murasame

   ![picture](https://vignette.wikia.nocookie.net/kancolle/images/7/7b/Murasame_Kai_Ni_Full.png/revision/latest/)


### A wrapper around MyWaifuList
https://mywaifulist.docs.stoplight.io/api-reference

### Documentation
You can find it [here](https://bitbucket.org/kashimabot/murasame/src/master/)